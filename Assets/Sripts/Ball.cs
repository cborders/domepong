﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(SphereCollider))]
[RequireComponent (typeof(Rigidbody))]
public class Ball : MonoBehaviour {

	public float _ballSpeed;

	private Rigidbody _rigidBody;

	private bool _running;

	public Transform _launcher;

	public void Start () {
		_rigidBody = GetComponent<Rigidbody> ();

		ResetRunning (_launcher);
	}

	public void Update () {
		if (!_running) {
			transform.position = _launcher.position;
		}
	}

	public void Launch () {
		_running = true;
		Vector3 direction = _launcher.forward.normalized;
		direction.Scale (new Vector3 (_ballSpeed, _ballSpeed, _ballSpeed));
		_rigidBody.velocity = direction;
	}

	public void ResetRunning (Transform launcher) {
		_running = false;
		_rigidBody.velocity = Vector3.zero;
		_launcher = launcher;
		transform.position = _launcher.position;
	}
}
