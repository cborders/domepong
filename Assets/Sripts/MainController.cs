﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Collider))]
public class MainController : MonoBehaviour
{
	public GameObject _player1;
	public GameObject _player2;
	public Ball _ball;

	public float _paddleSpeed;
	public float _skyboxSpeed;

	public Text _text;

	private Paddle.Player _turn;

	private int _player1Score;

	public int player1Score { get { return _player1Score; } }

	private int _player2Score;

	public int player2Score { get { return _player2Score; } }

	private bool _running;

	private static MainController _instance;

	public static MainController GetInstance()
	{
		if(_instance == null)
		{
			return new MainController();
		}
		else
		{
			return _instance;
		}
	}

	public MainController()
	{
		_instance = this;
	}

	public void TouchedBall(Paddle.Player player)
	{
		if(player == Paddle.Player.PLAYER_1)
		{
			_turn = Paddle.Player.PLAYER_2;
		}
		else
		{
			_turn = Paddle.Player.PLAYER_1;
		}
	}

	void Start()
	{
		_turn = Paddle.Player.PLAYER_1;
	}

	void Update()
	{
		float skyboxMotion = _skyboxSpeed * Time.deltaTime;
		float paddleMotion = _paddleSpeed * Time.deltaTime;

		transform.Rotate(0, skyboxMotion, 0);

		float player1 = Input.GetAxis("Player1");
		float player2 = Input.GetAxis("Player2");

		if(player1 != 0)
		{
			_player1.transform.Rotate(0, paddleMotion * player1, 0);
		}

		if(player2 != 0)
		{
			_player2.transform.Rotate(0, paddleMotion * player2, 0);
		}

		if(!_running && Input.GetButtonDown("Start"))
		{
			_running = true;
			_ball.Launch();
			_text.CrossFadeAlpha(0, 1, true);
		}
	}

	void OnTriggerExit(Collider other)
	{
		_running = false;
		Transform nextPlayer;

		if(_turn == Paddle.Player.PLAYER_1)
		{
			_player2Score++;
			nextPlayer = _player2.transform.FindChild("Launch");
		}
		else
		{
			_player1Score++;
			nextPlayer = _player1.transform.FindChild("Launch");
		}

		_text.text = "Player 1: " + _player1Score + "\nPlayer 2: " + _player2Score;
		_text.CrossFadeAlpha(1, 1, true);
		_ball.ResetRunning(nextPlayer);
	}
}
