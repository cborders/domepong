﻿using UnityEngine;

[RequireComponent (typeof(Collider))]
public class Paddle : MonoBehaviour
{
	public enum Player
	{
		PLAYER_1,
		PLAYER_2
	}

	public Player player;

	private MainController _controller;

	void Start()
	{
		_controller = MainController.GetInstance();
	}

	void OnTriggerEnter (Collider other)
	{
		_controller.TouchedBall(player);
	}
}
